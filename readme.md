# Veloci ESC

## Setup

You must be in python version 3.5 or 3.6 (FBS is not yet supported in 3.7)

Create a virtual environment in the current directory:

    python -m venv venv

Activate the virtual environment:

    # On Windows:
    venv\scripts\activate.bat

Install the required libraries:

    pip install -r requirements.txt


## Launch

To run application, execute the following command in this directory:

    fbs run
 
To freeze the application and create the installer, please check the [FBS tutorial page](https://github.com/mherrmann/fbs-tutorial)

## Projet structure

The project structure is the following

	/
	src
	- build: Settings for the application/installer building
		- ...
	- main: Sources and ressources for the application
		- icons: The icons for the app
		- python: The python sources
			- data: Classes related to data
				- bike_informations -> Class to reprensent the data gatered from the bike
			- hardware: Classes related to hardware 
				- eeprom -> Class to talk with the 24c08 from ST 
				- i2c -> Class to implement i2c over the FTDI chip
			- ihm: Graphical interfaces
				- ...
		- ressources: Images
	