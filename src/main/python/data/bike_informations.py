class BikeInformations:
	def __init__(self):
		self.serial_nb = 0 			# Serial
		self.soft_ver = "0.1" 		# Software version X.X format
		self.operating_time  = 0 	# Operating time in seconds 
		self.nominal_voltage = 0.0 	# Nominal voltage in V
		self.low_speed = 0 			# Low Speed in RPM
		self.med_speed = 0 			# Medium Speed in RPM
		self.high_speed = 0 		# High Speed in RPM
		self.limited_speed = False  # Is the speed limited ?
		self.temp_max_reached = 0   # The temperature maximum reached
	