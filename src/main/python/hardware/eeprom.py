from hardware.i2c import FTD232
import time
from data.bike_informations import BikeInformations


# Util to print a byte in base 10
def printBase10(byte):
    return str(byte2int(byte))


def byte2int(byte):
    return int.from_bytes(byte, byteorder='big')


class Eeprom():
    def __init__(self):
        self.ftd = FTD232()

    def isConnected(self):
        return self.ftd.verifyMPSSEMode()

    # Write 1 byte to the eeprom
    def write(self, address, data):
        if self.ftd:
            ret = False
            timeout = 0
            # Try to write while we don't get the ack (4 times max).
            while not ret and timeout < 4:
                self.ftd.setI2CStart()
                ret = self.ftd.sendByteAndCheckACK(0xA0)
                timeout += 1
                ret = self.ftd.sendByteAndCheckACK(address) & ret
                ret = self.ftd.sendByteAndCheckACK(data) & ret
                self.ftd.setI2CStop()
                self.ftd.setI2CLinesIdle()

            if timeout >= 4:
                return False

            return True
        else:
            return False

    # Read 1 byte from the eeprom
    def read(self, address):
        resp = None
        if self.ftd:
            self.ftd.setI2CStart()
            self.ftd.sendByteAndCheckACK(0xA0)
            self.ftd.sendByteAndCheckACK(address)
            self.ftd.setI2CLinesIdle()
            self.ftd.setI2CStart()
            ack = False
            timeout = 0
            while not ack and timeout <20:
                self.ftd.setI2CStart()
                ack = self.ftd.sendByteAndCheckACK(0xA0)
                ack = self.ftd.sendByteAndCheckACK(address) & ack
                self.ftd.setI2CLinesIdle()
                self.ftd.setI2CStart()
                ack = self.ftd.sendByteAndCheckACK(0xA1) & ack
                resp = self.ftd.readByteAndSendNAK()
                self.ftd.setI2CStop()
                timeout+=1

        if timeout >= 20:
            raise IOError("Impossible de lire la mémoire !")

        return resp

    # Read all the informations about the bike, return a BikeInformation object
    def readBikeInformations(self):
        infos = BikeInformations()
        infos.serial_nb = self.read(3) + self.read(2) + self.read(1) + self.read(0)  # Serial
        infos.soft_ver = printBase10(self.read(4)) + "." + printBase10(self.read(5))  # Software version X.X format
        infos.operating_time = byte2int(
            self.read(9) + self.read(8) + self.read(7) + self.read(6))  # Operating time in seconds
        infos.nominal_voltage = byte2int(self.read(11) + self.read(10)) / 1000  # Nominal voltage in V
        infos.low_speed = byte2int(self.read(13) + self.read(12))  # Low Speed in RPM
        infos.med_speed = byte2int(self.read(15) + self.read(14))  # Medium Speed in RPM
        infos.high_speed = byte2int(self.read(17) + self.read(16))  # High Speed in RPM
        if self.read(18) == b'\x00':  # Is the speed limited ?
            infos.limited_speed = False
        else:
            infos.limited_speed = True
        infos.temp_max_reached = byte2int(self.read(19))  # The temperature maximum reached

        return infos

    # Write low speed to eeprom
    def writeLowSpeed(self, lowSpeed):
        if lowSpeed < 0xFFFF:
            success = self.write(0x0c, lowSpeed & 0xFF)
            success = self.write(13, (lowSpeed >> 8) & 0xFF)& success
            return success
        else:
            return False

    # Write med speed to eeprom
    def writeMedSpeed(self, medSpeed):
        if medSpeed < 0xFFFF:
            success = self.write(14, medSpeed & 0xFF)
            success = self.write(15, (medSpeed >> 8) & 0xFF) & success
            return success
        else:
            return False

    # Write high speed to eeprom
    def writeHighSpeed(self, highSpeed):
        if highSpeed < 0xFFFF:
            success = self.write(16, highSpeed & 0xFF)
            success = self.write(17, (highSpeed >> 8) & 0xFF) & success
            return success
        else:
            return False

    # Write if the speed is limited to eeprom
    def writeLimitation(self, isLimited):
        if isLimited:
            return self.write(18, 1)
        else:
            return self.write(18, 0)
        return True
