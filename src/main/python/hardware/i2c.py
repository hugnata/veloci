import ftd2xx as ftd, time


def byte2int(byte):
    return int.from_bytes(byte, byteorder='big')


# The class representing the FTD232
class FTD232:

    # Set I2C lines (SDA/SCL) to Idle (High)
    def setI2CLinesIdle(self):
        if self.is_ok:
            self.device.write(bytes((0x80, 0xFF, 0xFB)))  # Setting all the lines to low level

    # Verifying if the MPSSEMode is active
    def verifyMPSSEMode(self):
        # Send invalid command, error return expected
        nb_sent = self.device.write(bytes((0xAA,)))  # nb_sent = number of byte sent to the FT232
        if not (nb_sent == 1):
            print("Impossible d'écrire sur le device")
            return False
        timeout = 0
        while (self.device.getQueueStatus() < 2 and timeout < 500):  # Waiting for the response
            timeout += 1
            time.sleep(0.001)

        if timeout < 500:
            resp = self.device.read(self.device.getQueueStatus())
            if not (resp[0] == 0xFA) and not (
                    resp[1] == 0xAA):  # In theory we should see \xFA (Bad command) followed by \xAA (our command)
                return False
        else:
            print("Impossible de synchroniser MPSSE avec la commande AA (timeout)")
            return False

        nb_sent = self.device.write(bytes((0xAB,)))  # nb_sent = number of byte sent to the FT232
        if not (nb_sent == 1):
            print("Impossible d'écrire sur le device")
            return False
        timeout = 0
        while self.device.getQueueStatus() < 2 and timeout < 500:  # Waiting for the response
            timeout += 1
            time.sleep(0.001)

        if timeout < 500:
            resp = self.device.read(self.device.getQueueStatus())
            if not (resp[0] == 0xFA) and not (
                    resp[1] == 0xAB):  # In theory we should see \xFA (Bad command) followed by \xAA (our command)
                return False
        else:
            print("Impossible de synchroniser MPSSE avec la commande AB (timeout)")
            return False

        return True

    def __init__(self):
        # Opening the self.device, raise ftd.self.deviceError if no ftd connected 
        self.device = ftd.open(0)

        # Configuring 
        self.device.setBitMode(0, 2)  # Enable MPSSE
        self.device.write(bytes((0x8A,)))  # Disable clock divide-by-5 for 60Mhz master clock
        self.device.write(bytes((0x97,)))  # Ensure adaptive clocking is off
        self.device.write(bytes((0x8C,)))  # Enable 3 phase data clocking, data valid on both clock edges for I2C
        self.device.write(bytes((0x9E, 0x07,
                                 0x00)))  # Enable drive-zero mode on the lines used for I2Con the bits AD0, 1 and 2 of the lower port, not required on the upper port AC 0-7
        self.device.write(bytes((0x85,)))  # Ensure internal loopback is off

        self.device.write(bytes((0x86, 0xFF, 0x00)))  # Set the clock frequency to high
        self.is_ok = self.verifyMPSSEMode()  # Verifying if the device is well configurated
        self.setI2CLinesIdle()  # Setting I2C lines (SDA/SCL) to idle (High)

    def setI2CStart(self):
        # We begin by setting the SDA & SCL Line low
        self.device.write(bytes((0x80, 0xFD, 0xFB)))
        self.device.write(bytes((0x80, 0xFC, 0xFB)))

    def setI2CStop(self):
        # We pull SDA low
        self.device.write(bytes((0x80, 0xFC, 0xFB)))
        # SCL goes High
        self.device.write(bytes((0x80, 0xFD, 0xFB)))
        # SDA goes High
        self.device.write(bytes((0x80, 0xFF, 0xFB)))

    def readByteAndSendNAK(self):
        # Clock one byte of data in
        cmd = bytes((0x20, 0x00, 0x00))

        # Clock out one bit (ACK/NACK). Here we NACK so this bit is = 1
        cmd += bytes((0x13, 0x00, 0xFF))

        # Put I2C line back to idle
        cmd += bytes((0x80, 0xFE, 0xFB))

        # Ask the MPSSE to send the results now
        cmd += bytes((0x87,))
        self.device.write(cmd)

        timeout = 0
        while self.device.getQueueStatus() == 0 and timeout < 500:  # Waiting for the response
            timeout += 1
            time.sleep(0.001)

        if timeout < 500:
            return self.device.read(1)
        else:
            return None

    def sendByteAndCheckACK(self, byte):
        # Clock out one byte (byte)
        cmd = bytes((0x11, 0x00, 0x00, byte))
        # Going back to idle
        cmd += bytes((0x80, 0xFE, 0xFB))
        # Clock one bit in
        cmd += bytes((0x22, 0x00, 0x87))
        self.device.write(cmd)

        timeout = 0
        while self.device.getQueueStatus() == 0 and timeout < 500:  # Waiting for the response
            timeout += 1
            time.sleep(0.001)

        if timeout < 500:
            ret = self.device.read(1)
            if byte2int(ret) & 0x01:
                return False
            else:
                return True
        else:
            return False
