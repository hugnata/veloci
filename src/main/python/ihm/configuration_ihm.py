from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from ftd2xx import DeviceError
from ihm.legal_information_ihm import *
from data.bike_informations import *
import datetime


def byteToString(byte):
    return str(byte).replace("b'", "").replace("'", "").replace("\\x", "").upper()

def speedIsValid(speed: int):
    return (speed < 65535) & (speed >= 0)

class ConfigurationIHM(QWidget):

    # Method to write data into the eeprom
    def writeInformations(self):
        self.info_msg.setText("Écriture en cours...")
        self.write_button.setEnabled(False)
        try:
            low = int(self.low.text())
            med = int(self.med.text())
            high = int(self.high.text())

            if speedIsValid(low) and speedIsValid(med) and speedIsValid(high):
                success = self.eeprom.writeLowSpeed(low)
                success = self.eeprom.writeMedSpeed(med) & success
                success = self.eeprom.writeHighSpeed(high) & success
                success = self.eeprom.writeLimitation(self.limitation.isChecked()) & success
                if success:
                    self.info_msg.setText("Écriture réussie")
                    self.info_msg.setStyleSheet("color: green;")
                else:
                    self.info_msg.setText("Écriture échouée")
                    self.info_msg.setStyleSheet("color: red;")
            else:
                self.info_msg.setText("La vitesse doit être positive et inférieure à 65 535")
                self.info_msg.setStyleSheet("color: red;")
        except DeviceError:
            self.info_msg.setText("Écriture échouée")
            self.info_msg.setStyleSheet("color: red;")
        self.write_button.setEnabled(True)

    def setEeprom(self, eeprom):
        self.info_msg.setText("")
        self.eeprom = eeprom
        self.updateValues()

    # Method used to refresh the ihm
    def updateValues(self):
        self.read_button.setEnabled(False)
        try:
            bike_infos = self.eeprom.readBikeInformations()

            self.hard_label.setText("Numéro de hardware: <b>" + byteToString(bike_infos.serial_nb) + "</b>")
            self.soft_label.setText("Version logicielle: <b>" + bike_infos.soft_ver + "</b>")
            self.time_label.setText(
                "Temps de fonctionnement: <b>" + str(datetime.timedelta(seconds=bike_infos.operating_time)) + "</b>")
            self.voltage_label.setText("Tension nominale: <b>" + str(bike_infos.nominal_voltage) + "</b> V")
            self.temp_label.setText("Température max atteinte : <b>" + str(bike_infos.temp_max_reached) + "°</b>")

            self.low.setText(str(bike_infos.low_speed))
            self.med.setText(str(bike_infos.med_speed))
            self.high.setText(str(bike_infos.high_speed))
            self.limitation.setChecked(bike_infos.limited_speed)
        except IOError:
            self.info_msg.setText("Impossible de lire les informations...")
            self.info_msg.setStyleSheet("color: red;")

        self.read_button.setEnabled(True)

    def __init__(self, appctxt):
        super().__init__()
        self.layout = QVBoxLayout()
        bike_infos = BikeInformations()

        image = QLabel()
        image.setAlignment(Qt.AlignCenter)
        pixmap = QPixmap(appctxt.get_resource('images/logo_durax400x100.jpg'))
        image.setPixmap(pixmap)
        # image.setStyleSheet("background-color: red")
        # image.setFixedSize(pixmap.width(), pixmap.height())

        ## INFORMATIONS
        informations = QWidget()
        information_layout = QVBoxLayout()

        info_label = QLabel("<b>Informations :</b>")
        # info_label.setStyleSheet("font-weight: bold;")
        information_layout.addWidget(info_label)

        self.hard_label = QLabel("Numéro de hardware: <b>" + byteToString(bike_infos.serial_nb) + "</b>")
        information_layout.addWidget(self.hard_label)

        self.soft_label = QLabel("Version logicielle: <b>" + bike_infos.soft_ver + "</b>")
        information_layout.addWidget(self.soft_label)

        self.time_label = QLabel(
            "Temps de fonctionnement: <b>" + str(datetime.timedelta(seconds=bike_infos.operating_time)) + "</b>")
        information_layout.addWidget(self.time_label)

        self.temp_label = QLabel(
            "Température max atteinte : <b>" + str(bike_infos.temp_max_reached) + "°</b>")
        information_layout.addWidget(self.temp_label)

        self.voltage_label = QLabel("Tension nominale: <b>" + str(bike_infos.nominal_voltage) + "</b> V")
        information_layout.addWidget(self.voltage_label)

        informations.setLayout(information_layout)

        ## Paramètres
        parametres = QWidget()
        parametres_layout = QFormLayout()

        param_label = QLabel("<b>Paramètres :</b>")
        parametres_layout.addRow(param_label)

        low_label = QLabel("Vitesse basse (en rpm): ")
        self.low = QLineEdit(str(bike_infos.low_speed))
        parametres_layout.addRow(low_label, self.low)

        med_label = QLabel("Vitesse intermédiaire (en rpm): ")
        self.med = QLineEdit(str(bike_infos.med_speed))
        parametres_layout.addRow(med_label, self.med)

        high_label = QLabel("Vitesse haute (en rpm): ")
        self.high = QLineEdit(str(bike_infos.high_speed))
        parametres_layout.addRow(high_label, self.high)

        limitation_label = QLabel("Désactiver la vitesse limite de 25km/h: ")
        self.limitation = QCheckBox()
        self.limitation.setTristate(False)
        self.limitation.setChecked(bike_infos.limited_speed)
        parametres_layout.addRow(limitation_label, self.limitation)

        caption_label = QLabel("\n\n\n\nEn cochant cette case, vous autorisez votre vélo à dépasser les 25 km/h et reconnaissez être informé que celui-ci entre alors dans la catégorie du cyclomoteur qui nécessite le permis AM, une immatriculation, une assurance, le port du casque, de gants….")
        caption_label.setTextFormat(Qt.RichText)
        caption_label.setWordWrap(True)
        caption_label.setAlignment(Qt.AlignHCenter)

        parametres_layout.addRow(caption_label)
        parametres.setLayout(parametres_layout)

        ## Buttons
        buttons = QWidget()
        buttons_layout = QHBoxLayout()

        self.read_button = QPushButton("Lecture des paramètres")
        self.read_button.clicked.connect(lambda: self.updateValues())
        self.write_button = QPushButton("Ecriture des paramètres")
        self.write_button.clicked.connect(lambda: self.writeInformations())

        buttons_layout.addWidget(self.read_button)
        buttons_layout.addWidget(self.write_button)

        buttons.setLayout(buttons_layout)

        self.info_msg = QLabel("")
        self.info_msg.setAlignment(Qt.AlignCenter)

        version = QLabel("Version 0.1")
        version.setAlignment(Qt.AlignCenter)

        self.layout.addWidget(image)
        self.layout.addWidget(informations)
        self.layout.addWidget(parametres)
        self.layout.addWidget(buttons)


        self.layout.addWidget(self.info_msg)
        self.layout.addWidget(version)
        self.setLayout(self.layout)
        self.show()
