# coding: utf8
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class LegalInformationIHM(QWidget):
    def __init__(self, appctxt):
        super().__init__()

        layout = QVBoxLayout()

        # The Veloci Logo
        image = QLabel()
        pixmap = QPixmap(appctxt.get_resource('images/logo_durax400x100.jpg'))
        image.setPixmap(pixmap)
        image.setFixedSize(pixmap.width(), pixmap.height())
        image.setAlignment(Qt.AlignHCenter)
        image.setStyleSheet("background-color: red;")

        # The legal informations
        text = QWidget()
        text_layout = QVBoxLayout()

        text_layout.addWidget(image)

        informations_legales = QLabel("Informations légales:")
        informations_legales.setStyleSheet("font-weight: bold;")
        informations_legales.setAlignment(Qt.AlignHCenter)
        text_layout.addWidget(informations_legales)

        siret_nb = QLabel("Siret: <b>818 525 719 00022</b>")
        text_layout.addWidget(siret_nb)
        contact = QLabel("Contact: <b>contact@veloci-industries.com</b>")
        text_layout.addWidget(contact)
        self.setFixedHeight(250)
        text.setLayout(text_layout)


        #layout.addWidget(image)
        layout.addWidget(text)
        self.setLayout(text_layout)
