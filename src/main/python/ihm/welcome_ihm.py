# coding: utf8
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import os



class WelcomeIHM (QWidget):
	def __init__(self,appctxt,msg):
		super().__init__()
		print(os.getcwd())

		self.container = QWidget(self)

		layout = QVBoxLayout()
		self.label2 = QLabel()
		pixmap = QPixmap(appctxt.get_resource('images/logo_durax400x100.jpg'))
		self.label2.setPixmap(pixmap)
		self.label2.setAlignment(Qt.AlignHCenter)

		self.label_nonconnecte = QLabel("\n\n\n"+ msg, self)
		self.label_nonconnecte.setTextFormat(Qt.RichText)
		self.label_nonconnecte.setTextInteractionFlags(Qt.TextBrowserInteraction)
		self.label_nonconnecte.setOpenExternalLinks(True)
		self.label_nonconnecte.resize(430,30)
		self.label_nonconnecte.setWordWrap(True)
		self.label_nonconnecte.setAlignment(Qt.AlignHCenter)

		layout.addWidget(self.label2)
		layout.addWidget(self.label_nonconnecte)
		#self.container.setAlignment(Qt.AlignVCenter)
		self.container.resize(430, 400)
		#self.container.setStyleSheet("background-color: red;")
		self.container.setLayout(layout)
		self.show()
