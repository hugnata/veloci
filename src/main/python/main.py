# coding: utf8

import sys
import time
import threading
from PyQt5.QtWidgets import QMainWindow, QTabWidget, QVBoxLayout
# Beware ! fbs is only available for python 3.5 and 3.7 for the moment (11/2019)
from fbs_runtime.application_context.PyQt5 import ApplicationContext
from ihm.legal_information_ihm import LegalInformationIHM
from ihm.welcome_ihm import WelcomeIHM



def probeEeprom():
    """
    Thread to probe the eeprom, to see when a bike connect/disconnects
    """
    connected = False
    t = threading.current_thread()
    while getattr(t, "do_run", True):
        # If a bike is connected
        if connected:
            # We try to communicate with it
            try:
                eeprom.isConnected()
                time.sleep(0.5)
            # If we couldnt communicate with him, a DeviceError exception is thrown
            except:
                tabs.insertTab(0, welcome_page, "Configuration")
                tabs.removeTab(1)
                tabs.setCurrentIndex(0)
                connected = False
        else:
            # We try to communicate with it
            try:
                eeprom = Eeprom()

                config_ihm.setEeprom(eeprom)
                tabs.insertTab(0, config_ihm, "Configuration")
                tabs.removeTab(1)
                tabs.setCurrentIndex(0)
                connected = True
            # If we couldnt communicate with it, a DeviceError exception is thrown
            except:
                time.sleep(0.1)


if __name__ == '__main__':

    # ---------- Window creation
    appctxt = ApplicationContext()  # 1. Instantiate ApplicationContext

    window = QMainWindow()
    window.setWindowTitle("Veloci bike configurator")
    window.setFixedSize(430, 660)
    window.move(300, 50)
    window.show()

    tabs = QTabWidget(window)
    tabs.resize(430, 660)


    # ---------- Searching for driver
    try:
        # The import will fail and raise an error if the driver isn't installed
        import ftd2xx as ftd

        # If we succed to load the driver, we are now waiting for the bike
        welcome_page = WelcomeIHM(appctxt,
                                   "Connectez le vélo à votre ordinateur à l'aide du cable fourni pour voir ses statistiques et modifier ses paramètres", )
        driverFound = True
    except:
        # If the driver isn't installed, we ask the user to install it
        welcome_page = WelcomeIHM(appctxt,
                                   "Impossible de trouver FTD2XX.dll. Avez vous installé le driver disponible à l'adresse suivante ? : \n <br> <a href='https://www.ftdichip.com/Drivers/D2XX.htm'> https://www.ftdichip.com/Drivers/D2XX.htm</a>")

        driverFound = False

    # Legal information page is created
    legal_informations = LegalInformationIHM(appctxt)

    tabs.addTab(welcome_page, "Configuration")
    tabs.addTab(legal_informations, "Informations légales")

    tabs.show()

    # If we found a driver, we can load the Configuration ihm
    if driverFound:
        from ihm.configuration_ihm import ConfigurationIHM
        from hardware.eeprom import Eeprom

        # We launch the probe thread to probe the eeprom state and switch from welcome_ihm to configurationIHM
        probe_thread = threading.Thread(target=probeEeprom)
        probe_thread.start()

        config_ihm = ConfigurationIHM(appctxt)
        config_ihm.hide()

    else:
        probe_thread = False

    exit_code = appctxt.app.exec_()  # 2. Invoke appctxt.app.exec_()
    if probe_thread:
        probe_thread.do_run = False
    sys.exit(exit_code)
